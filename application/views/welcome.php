<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css">

    <style>
        body {
            padding-top: 5rem;
        }
    </style>

    <title>Welcome</title>
</head>

<body>
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <div class="container">
            <a class="navbar-brand" href="#">BEM-UBSI</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="<?php echo base_url();?>">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url().'pendaftaran/create'; ?>">Pendaftaran</a>
                    </li>

                </ul>
                <ul class="nav navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url().'auth'; ?>">Login</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <!-- Main jumbotron for a primary marketing message or call to action -->
                <div class="jumbotron">
                    <div class="container">
                    <h1 class="display-3">Selamat Datang!</h1>
                    <p>Bagi calon anggota Badan Eksekutif Mahasiswa - BEM yang akan menjadi anggota baru silahkan melakukan pendaftaran dengan cara klik tombol dibawah. Terima Kasih.</p>
                    <p><a class="btn btn-primary btn-lg" href="<?php echo base_url().'pendaftaran/create'; ?>" role="button">Daftar &raquo;</a></p>
                    </div>
                </div>        
            </div>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/jquery/jquery-3.3.1.js"></script>
    <script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
</body>

</html>