<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/datatables/css/dataTables.bootstrap4.min.css">

    <title>BEM - UBSI</title>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container">
            <a class="navbar-brand" href="#">BEM-UBSI</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="<?php echo base_url().'mahasiswa/'; ?>">Index <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url().'mahasiswa/create'; ?>">Input Data</a>
                    </li>

                </ul>
                <ul class="nav navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url().'auth/logout'; ?>">Logout</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 align="center">Data Pendaftaran </h2>
                <div class="table-responsive">
                    <table class="table" id="table_mhs">
                        <thead class="thead-dark">
                            <tr>
                                <th>NIM</th>
                                <th>Nama</th>
                                <th>Jenis Kelamin</th>
                                <th>Jurusan</th>
                                <th>Email</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($mahasiswa as $mhs) { ?>
                            <tr>
                                <td>
                                    <?php echo $mhs->nim ?>
                                </td>
                                <td>
                                    <?php echo $mhs->nama ?>
                                </td>
                                <td>
                                    <?php echo $mhs->jk ?>
                                </td>
                                <td>
                                    <?php echo $mhs->jurusan ?>
                                </td>
                                <td>
                                    <?php echo $mhs->email ?>
                                </td>
                                <td>
                                    <a class="btn btn-outline-warning btn-sm" href="<?php echo base_url().'mahasiswa/edit/'.$mhs->nim; ?>">Edit</a>
                                    <a class="btn btn btn-outline-danger btn-sm" href="<?php echo base_url().'mahasiswa/delete/'.$mhs->nim; ?>">Hapus</a>
                                </td>

                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/jquery/jquery-3.3.1.js"></script>
    <script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>assets/datatables/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/datatables/js/dataTables.bootstrap4.min.js"></script>
    <script type="text/javascript">
        $(document).ready( function () {
            $('#table_mhs').DataTable();
        } );
    </script>
</body>

</html>