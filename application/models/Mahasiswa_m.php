<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

	class Mahasiswa_m extends CI_ModeL{

	public function getAllMahasiswa(){
		$result = $this->db->get('mahasiswa');
		return $result->result();
	}

	public function saveMahasiswa(){
		$data = array(
		'nim'=> $this->input->post('nim'),
		'nama'=> $this->input->post('nama'),
		'jk'=> $this->input->post('jk'),
		'jurusan'=> $this->input->post('jurusan'),
		'email'=> $this->input->post('email')
		);

		return $this->db->insert('mahasiswa', $data);			
	}

	public function getMahasiswa($id){
		$this->db->where('nim', $id);
		$result = $this->db->get('mahasiswa');
		return $result->row();
	}

	public function updateMahasiswa($id){
		$data = array(
			'nama' => $this->input->post('nama'),
			'jk' => $this->input->post('jk'),
			'jurusan' => $this->input->post('jurusan'),
			'email' => $this->input->post('email')
			);

		$this->db->where('nim', $id);
		return $this->db->update('mahasiswa', $data);
	}

	public function deleteMahasiswa($id){
		$this->db->where('nim', $id);
		return $this->db->delete('mahasiswa');
	}
}

 ?>