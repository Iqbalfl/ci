<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pendaftaran extends CI_Controller {

    function __construct(){
		parent::__construct();
		
		$this->load->model('Mahasiswa_m','mahasiswa');
	}

	public function create()
	{
		$this->load->view('pendaftaran/create');
    }
    
    public function store()
    {
        $result = $this->mahasiswa->saveMahasiswa();
		if ($result){
			$this->load->view('pendaftaran/response');
		}
    }
}
