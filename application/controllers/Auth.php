<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Auth_m', 'auth');
    }

    public function index()
    {
        $this->load->view('login');
    }

    public function login()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $where = array(
            'username' => $username,
            'password' => md5($password),
        );
        
        $check = $this->auth->login("admin", $where)->num_rows();
        
        if ($check > 0) {
            $user = $this->auth->login("admin", $where)->row();
            
            $data_session = array(
                'name' => $user->name,
                'email' => $user->email,
                'status' => "login",
            );

            $this->session->set_userdata($data_session);
            
            redirect(base_url('mahasiswa'));

        } else {
            echo "Username dan password salah !";
        }
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect(base_url('auth'));
    }
}
