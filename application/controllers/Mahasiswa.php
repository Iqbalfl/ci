<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Mahasiswa extends CI_Controller {

	//constructor
	function __construct(){
		parent::__construct();

		// cek apakah user sudah login
		if ($this->session->userdata('status') != "login") {
            redirect(base_url("auth"));
		}
		
		$this->load->model('Mahasiswa_m','mahasiswa');
	}

	public function index(){
		$data['mahasiswa'] = $this->mahasiswa->getAllMahasiswa();
		$this->load->view('mahasiswa/index', $data);
	}

	public function create(){
		$data['header'] = "Tambah Mahasiswa";
		$this->load->view('mahasiswa/create', $data);
	}

	public function store(){
		$result = $this->mahasiswa->saveMahasiswa();
		if ($result){
			redirect('/mahasiswa/index');
		}
	}

	public function edit($id){
		$data['mahasiswa'] = $this->mahasiswa->getMahasiswa($id);
		$this->load->view('mahasiswa/edit', $data);
	}

	public function update($id){
		$result = $this->mahasiswa->updateMahasiswa($id);
		if($result){
			redirect('/mahasiswa/index');
		}
	}

	public function delete($id){
		$result = $this->mahasiswa->deleteMahasiswa($id);
		if ($result){
			redirect ('/mahasiswa/index');
		}
	}

}

?> 